<?php

$sentences = [
    "Anh ơi trái đất dẫu tròn, anh trốn không kỹ là còn gặp em.",
    "Hoa chỉ nở khi có người tưới nước. Em chỉ cười khi đứng trước người em yêu.",
    "Đừng gọi em là thiếu nữ. Trong khi thứ em thiếu là anh.",
    "Nhà em gần miếu, gần chùa. Anh không yêu, em cũng bỏ bùa cho yêu.",
    "Em yêu anh không hề nói phét. Tình chúng mình căng đét phải không anh?",
    "Bầu trời màu xanh, tim em màu đỏ. Sao anh chưa chịu, ngỏ lời yêu em.",
    "Anh không yêu em thì em yêu người khác. Luật giang hồ không cho phép em cô đơn.",
    "Sống ảo thì em có nhưng sống cùng ai đó... thì em chưa.",
    "Tóc em màu đen. Tim em màu đỏ. Lại gần em hỏi nhỏ? Anh có thích em không?",
    "Trái tim em vốn đã hỗn độn. Sao anh còn đến làm lộn xộn thêm?",
    "Nếu anh coi tình em là hạt cát. Em sẽ đổ cả sa mạc vào tim anh.",
    "Cuộc đời em còn đang dang dở. Anh bước vào che chở có được không?",
    "Đừng yêu em vì em là thuốc phiện. Nghiện vào rồi là không bỏ được đâu.",
    "Anh chỉ việc yêu em thôi còn cả thế giới cứ để em lo.",
    "Nếu cảm thấy mệt quá, em cho mượn bờ vai. Thì thầm em nói nhỏ, yêu em thì không sai.",
    "Nếu em là thuốc phiện? Anh có nguyện yêu em?",
    "Có rất nhiều cách để được hạnh phúc. Nhưng cách nhanh nhất là được nhìn thấy anh.",
    "Anh vô gia cư hay sao mà cứ ở trong tim em mãi thế?",
    "Lồng thì em để nhốt chim. Còn anh em nhốt trong tim đây này.",
    "Trời sinh yếu đuối mỏng manh nên cần được tựa vai anh mỗi ngày.",
    "Gặp anh em bỗng muốn 'Thờ'. Không 'Thờ Ích Thích' mà 'Thờ Ương Thương'.",
    "Anh mặc gì cũng được nhưng không được mặc kệ em.",
    "Trời xanh gió thổi chẳng ngừng. Anh không cẩn thận coi chừng yêu em.",
    "Bao nhiêu cân thính cho vừa. Bao nhiêu cân bả mới lừa được anh.",
    "Đôi khi muốn giả làm gà. Để xem anh thịt hay là anh nuôi?",
    "Em không biết chia tay bởi vì em dốt Toán. Đừng bắt em thích Hóa vì em chỉ thích Anh.",
    "Ngoài kia ai thích chiều tà, còn em chỉ thích như là chiều anh.",
    "Hè về nắng gắt cháy da. Bên ai cũng nóng hay là bên em.",
    "Cầm vàng đừng để vàng rơi. Anh ơi anh rớt người yêu rồi này.",
    "Trái tim em giờ đầy axit. Cần anh dung hòa bằng một ít bazo.",
    "Say bia say rượu cũng thường. Sao anh không thử chọn đường say em?",
    "Em yêu anh với vận tốc không phanh. Dù mong manh nhưng anh là tất cả.",
    "Ngàn năm nước chảy chẳng dừng. Em đây cũng chẳng thể ngừng yêu anh.",
    "Xe mà không dừng lại. Có thể do đứt phanh. Em yêu không dừng lại. Chắc chắn là tại anh.",
    "Ship em một cốc trà đào. Tiện cho em hỏi lối vào tim anh?"
];


function getRandomSentence($sentences) {
    $index = array_rand($sentences);
    return $sentences[$index];
}

$rawJson = file_get_contents('php://input');

$update = json_decode($rawJson, true);

try {
    $command = $update['message']['text'];
    $chatId = $update['message']['chat']['id'];
} catch (Exception $exception) {
    header('Content-Type: application/json');
    echo json_encode([
        'message' => 'no-data'
    ]);
}

switch ($command) {
    case 'HELLO';
        $response = sendMessage('Chào !', $chatId);
        break;
    case "PULL":
        $output = shell_exec('git pull origin master');
        $response = sendMessage($output, $chatId);
        $response = sendMessage(getRandomSentence($sentences), $chatId);
        header('Content-Type: application/json');
        echo json_encode($response);
        break;
    case "Mày có biết bố mày là ai không ?":
        $response = sendMessage('Câu trả lời đấy chính là: dù bố mày có là ai thì vẫn phải thượng tôn pháp luật. Nghe nói camera của các chú công an xịn và đẹp lắm. Chắc một chút nữa thôi hình ảnh của hai cháu sẽ được cả nước biết tới, như một tấm gương dành cả đời để bắt người khác đi tìm bố hộ mình :(((', $chatId);
        break;
    case "Thùy Linh ơi đẩy code cho anh":
        $output = shell_exec('git pull origin master');
        $output = "Dạ được anh ơi !. \n" . $output;
        $response = sendMessage($output, $chatId);
        $response = sendMessage(getRandomSentence($sentences), $chatId);
        header('Content-Type: application/json');
        echo json_encode($response);
        break;
    default:
        break;
}

function sendMessage($message, $chatId)
{
    $token = '6852061516:AAGh8cbpuyUbarON_yKtfg2Q3vjrBCoKxGo';

    $requestUrl = 'https://api.telegram.org/bot' . $token . '/sendMessage';

    $postData = [
        'chat_id' => $chatId,
        'text' => $message,
    ];

    $curl = curl_init($requestUrl);

    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_POST, true);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $postData);

    $response = curl_exec($curl);

    if ($response === false) {
        $error = curl_error($curl);
    }

    curl_close($curl);

    return $response;
}